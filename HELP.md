# Micro-service Composite Banque Comptes-Clients
Ce micro-service <b>composite</b> gère l'association de comptes bancaires à des comptes clients pour la banque version micro-services.

## References
* Documentation v1.0
* Projet v7.0

## Environnement
* Spring v2.6.4
* Spring cloud v2021.0.1 +
    * io.micrometer/micrometer-registry-prometheus
    * spring-cloud-starter-zipkin
    * OpenFeign
* Lombok
* Java 8

## Fonctions (exposées) et Transient Objects
*
* Ref : TO DO : voir swagger
* [Swagger] (https://docs.spring.io/spring-boot/docs/2.6.4/maven-plugin/reference/html/#build-image)

## Elements d'architecture inclus
* Annuaire
* Monitoring (dont exposition Prometheus)
* Distributed Tracing (via Zipkin)
* Externalisation de configuration
* Load Balancing

## Elements de configuration
Ce micro-service lance le service d'interrogation des comptes clients liés au comptes bancaites associés avec les éléments de configuration suivants :
* Port 10031
* Niveau de Log sur INFO
* Profil dev
* Serveur de configuration disponible en localhost sur port 10003
* Zipkin disponible en localhost sur port 9411
* Annuaire disponible en localhost sur port 10001
* Services de gestion des comptes clients disponible au travers de l'annuaire via l'ID de service clientsservice
* Services de gestion des comptes bancaires disponible au travers de l'annuaire via l'ID de service comptesservice

## Construction et lancement
Rien à signaler.
* Pour construire une image docker :
    * il est possible d'utiliser le goal spring-boot:build-image
* Pour lancer l'image docker :
    * Ne pas oublier d'exposer les ports

## Notes et remarques
* Le fichier src/main/java/resources/solo.yml
    * Renommé en application.yml, il permet de se passer du service de configuration
* Il ne peut y avoir de '-' dans le nom de ce service à cause du service de configuration
